# Revolut BTC to EUR exchange calculator #

Simple wrapper that calculate current earnings base on initial buy price and amount

# Example #

provide INITIAL VALUE, AMOUNT in float type

```

calc := revolutCalc.NewCalculator()
calc.StartChecking( {INITIAL VALUE}, {INITIAL AMOUNT})

```

then simply run from cli

```
go run main.go
```