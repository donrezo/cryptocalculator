package revolutCalc

import (
	"cryptocalculator/bitstamp"
	"fmt"
	"strconv"
)

type Calculator struct {
	client *bitstamp.BitStamp
}

type BTC struct {
	value  float64
	amount float64
}

func NewCalculator() *Calculator {
	c := bitstamp.NewClient()
	return &Calculator{client: c}
}

func (calc *Calculator) StartChecking(value, amount float64) {
	coin := BTC{
		value:  value,
		amount: amount,
	}

	for {
		c := calc.client.GetBtcValue()
		f, err := strconv.ParseFloat(c.Last, 64)
		if err != nil {
			fmt.Println(err)
		}
		spread, exchangeRateRevolut, pocket, minimalExchangeProfitRate := coin.CalculateCurrentValues(f)

		fmt.Printf(
			"Obecny spread: %v EUR. "+
				"Obecny kurs: %v EUR"+
				", Obecny pocket: %v EUR, "+
				"Minimalny kurs zwrotu: %v EUR \n",
			spread,
			exchangeRateRevolut,
			pocket,
			minimalExchangeProfitRate)
	}
}

func (c *BTC) CalculateCurrentValues(currentVal float64) (float64, float64, float64, float64) {
	spreadCost := currentVal / 100 * 1.5
	revolutExchange := currentVal - spreadCost
	currentVa := (revolutExchange * c.amount) - c.value
	pocket := c.amount * revolutExchange

	return currentVa, currentVal - spreadCost, pocket, c.min(currentVal)
}

func (c *BTC) min(currentVal float64) float64 {
	var m float64

	for {
		spreadCost := currentVal / 100 * 1.5
		revolutExchange := currentVal - spreadCost
		currentVal++
		x := c.amount * revolutExchange

		if c.value < x {
			m = currentVal - spreadCost
			break
		}
	}

	return m
}
