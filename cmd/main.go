package main

import "cryptocalculator/revolutCalc"

func main() {
	calc := revolutCalc.NewCalculator()
	calc.StartChecking(5000, 0.7)
}
