package bitstamp

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type BitStamp struct {
	client *http.Client
}

type BTCEUR struct {
	Last string `json:"last"`
}

func NewClient() *BitStamp {
	c := http.Client{
		Transport:     nil,
		CheckRedirect: nil,
		Jar:           nil,
		Timeout:       0,
	}
	return &BitStamp{client: &c}
}

func (c *BitStamp) GetBtcValue() BTCEUR {
	var btceur BTCEUR
	buf, err := c.client.Get("https://www.bitstamp.net/api/v2/ticker/btceur");
	if err != nil {
		fmt.Println(err)
	}

	defer buf.Body.Close()

	e := json.NewDecoder(buf.Body).Decode(&btceur)
	if e != nil {
		fmt.Println(e)
	}

	return btceur
}

